/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elecfant.cardgamedesktop;

import com.elecfant.cardgamedesktop.cards.Card;
import com.elecfant.cardgamedesktop.cards.CardList;
import com.elecfant.cardgamedesktop.cards.Deck;
import com.elecfant.cardgamedesktop.cards.GameEntity;
import com.golden.gamedev.GameObject;
import com.golden.gamedev.gui.TButton;
import com.golden.gamedev.gui.toolkit.FrameWork;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Drags
 */
class CGDGame extends GameObject implements CGDConstants {

  private GameEntity entityDB[];
  private int snapped_entity = -1;
  private boolean tutorial_shown = false;
  private FrameWork gui;

  public CGDGame(CGDEngine parent) {
    super(parent);
    setFPS(30);
  }

  @Override
  public void initResources() {
    gui = new FrameWork(bsInput, getWidth(), getHeight());
    gui.add(new TButton("Add Card", 10, 10, 150, 30) {
      @Override
      public void doAction() {
        super.doAction();
        JFrame od = new JFrame();
        JFileChooser fileDialog = new JFileChooser();
        fileDialog.setDialogTitle("Select card foreground");
        fileDialog.setFileFilter(new FileNameExtensionFilter("Image files", "png", "jpg", "bmp"));
        int openChoice = fileDialog.showOpenDialog(od);
        if (openChoice == JFileChooser.APPROVE_OPTION) {
          File selectedFile = fileDialog.getSelectedFile();
          fileDialog.setDialogTitle("Select card foreground");
          fileDialog.setFileFilter(new FileNameExtensionFilter("Image files", "png", "jpg", "bmp"));
          int openChoice2 = fileDialog.showOpenDialog(od);
          if (openChoice2 == JFileChooser.APPROVE_OPTION) {
            File selectedFile2 = fileDialog.getSelectedFile();
            Card card = CardList.create_card(selectedFile.getPath(), selectedFile2.getPath());
            add_entity(card);
          }
        }
      }
    });
    gui.add(new TButton("Create Deck", 10, 45, 150, 30) {
      @Override
      public void doAction() {
        super.doAction();
        Deck deck = new Deck();
        deck.set_pos(100, 100);
        deck.set_scale(0.3);
        deck.set_size(600, 900);
        add_entity(deck);
      }
    });
    gui.validateUI();
    CardList.set_defaults(null, pre_assets + "heroes.png");
    entityDB = new GameEntity[0];

  }

  @Override
  public void update(long elapsedTime) {
    if (bsInput.isKeyReleased(KeyEvent.VK_F1)) {
      this.tutorial_shown = !this.tutorial_shown;
      snapped_entity = -1;
    }
    if (bsInput.isKeyReleased(KeyEvent.VK_ESCAPE)) {
      parent.nextGameID = 0;
      this.finish();
    }
    if (bsInput.isMouseDown(1)) {  // left mouse button
      int x = bsInput.getMouseX();
      int y = bsInput.getMouseY();
      if (gui.getHoverComponent() != null) {
        if ("Panel.TitleBar".equals(gui.getHoverComponent().UIName())) {
          gui.update();
          return;
        }
      }
      if (this.snapped_entity == -1) {
        for (int i = entityDB.length - 1; i >= 0; --i) {
          if (entityDB[i].inside(x, y)) {
            bring_to_front(i);
            this.snapped_entity = entityDB.length - 1;
            boolean shift = bsInput.isKeyDown(KeyEvent.VK_SHIFT);
            boolean control = bsInput.isKeyDown(KeyEvent.VK_CONTROL);
            if ((entityDB[this.snapped_entity] instanceof Deck) && !shift && !control) {
              add_entity(((Deck) entityDB[this.snapped_entity]).pop());
              this.snapped_entity = entityDB.length - 1;
            }
            break;
          }
        }
      }


      if (this.snapped_entity != -1) {
        if (bsInput.isKeyDown(KeyEvent.VK_CONTROL)) {
          int dy = bsInput.getMouseDY();
          int dx = bsInput.getMouseDX();
          entityDB[this.snapped_entity].scale(1 + (double) dy / 100);
          // TODO correct rotation/click detection
          // entityDB[this.snapped_entity].rotate((double) dx / 100 * Math.PI);
        } else {
          entityDB[this.snapped_entity].move(bsInput.getMouseDX(), bsInput.getMouseDY());
        }
      }
    } else if (bsInput.isMousePressed(3)) { // right mouse button
      int x = bsInput.getMouseX();
      int y = bsInput.getMouseY();
      for (int i = entityDB.length - 1; i >= 0; --i) {
        if (entityDB[i].inside(x, y)) {
          if (entityDB[i] instanceof Card) {
            ((Card) entityDB[i]).flip();
          }
          if (entityDB[i] instanceof Deck) {
            ((Deck) entityDB[i]).shuffle();
          }
          break;
        }
      }
    } else if (bsInput.isMouseReleased(1)) {
      if (snapped_entity != -1) {
        if (entityDB[snapped_entity] instanceof Card) {
          int x = bsInput.getMouseX();
          int y = bsInput.getMouseY();
          boolean put_in_deck = false;
          for (int i = entityDB.length - 2; i >= 0; --i) {
            if (entityDB[i].inside(x, y) && entityDB[i] instanceof Deck) {
              ((Deck) entityDB[i]).add_card((Card) entityDB[snapped_entity]);
              pop_entity();
              put_in_deck = true;
              break;
            }
          }
          if (!put_in_deck) {
            ((Card)entityDB[snapped_entity]).set_facing(true);
          }
        }
      }
      snapped_entity = -1;
    }
    gui.update();
  }

  @Override
  public void render(Graphics2D g) {
    g.setBackground(background_color);
    g.clearRect(0, 0, getWidth(), getHeight());
    for (int i = 0; i < entityDB.length; ++i) {
      if ((snapped_entity != -1) && (i == entityDB.length - 1)) {
        gui.render(g);
      }
      entityDB[i].render(g);
    }
    if (this.tutorial_shown) {
      String[] tutorial = new String[]{"F1 - show/hide this screen",
        "Left Click and Drag to move cards",
        "Shift+Left Click and Drag to move decks",
        "Left Click and Drag to draw card from deck",
        "Right Click card to flip it",
        "Right Click deck to shuffle it",
        "Left Click and Drag card onto Deck to put that card on top of that deck",
        "Control+Left Click and drag anything vertically to resize"};
      g.setFont(new Font("Calibri", Font.BOLD, 22));
      int h = g.getFontMetrics().getHeight();
      int th = h * tutorial.length;
      int w = 0;
      for (int i = 0; i < tutorial.length; ++i) {
        if (g.getFontMetrics().stringWidth(tutorial[i]) > w) {
          w = g.getFontMetrics().stringWidth(tutorial[i]);
        }
      }
      g.setColor(new Color(255, 255, 255, 200));
      g.fillRect(170, 30 - h, w, th);
      for (int i = 0; i < tutorial.length; ++i) {
        g.setColor(Color.BLACK);
        g.drawString(tutorial[i], 170, 30 + i * h);

      }
    }
    if (snapped_entity == -1) {
      gui.render(g);
    }
  }

  private void bring_to_front(int i) {
    GameEntity temp = entityDB[i];
    for (int j = i; j < entityDB.length - 1; ++j) {
      entityDB[j] = entityDB[j + 1];
    }
    entityDB[entityDB.length - 1] = temp;
  }

  private void add_entity(GameEntity card) {
    if (card == null) {
      return;
    }
    GameEntity[] temp = new GameEntity[entityDB.length + 1];
    System.arraycopy(entityDB, 0, temp, 0, entityDB.length);
    entityDB = temp;
    entityDB[entityDB.length - 1] = card;
  }

  private void pop_entity() {
    if (entityDB == null || entityDB.length == 0) {
      return;
    }
    GameEntity[] temp = new GameEntity[entityDB.length - 1];
    System.arraycopy(entityDB, 0, temp, 0, entityDB.length - 1);
    entityDB = temp;
  }
}
