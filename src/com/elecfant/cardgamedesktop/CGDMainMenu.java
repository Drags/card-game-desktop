/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elecfant.cardgamedesktop;

import com.golden.gamedev.GameObject;
import com.golden.gamedev.gui.TButton;
import com.golden.gamedev.gui.toolkit.FrameWork;
import java.awt.Graphics2D;

/**
 *
 * @author Drags
 */
public class CGDMainMenu extends GameObject implements CGDConstants {

  FrameWork gui;
  TButton bttn_table;
  TButton bttn_manage;
  TButton bttn_exit;

  public CGDMainMenu(CGDEngine parent) {
    super(parent);
    setFPS(30);
    this.showCursor();
  }

  @Override
  public void initResources() {

    bttn_table = new TButton("Table", getWidth() / 2 - 150, getHeight() / 2 - 30, 300, 60) {
      @Override
      public void doAction() {
        super.doAction();
        parent.nextGameID = 1;
        finish();
      }
    };

    bttn_manage = new TButton("Manage", getWidth() / 2 - 150, getHeight() / 2 + 40, 300, 60) {
      @Override
      public void doAction() {
        super.doAction();
        parent.nextGameID = 2;
        CGDMainMenu.this.finish();
      }
    };
    
    bttn_exit = new TButton("Exit", getWidth() / 2 - 150, getHeight() / 2 + 110, 300, 60) {
      @Override
      public void doAction() {
        super.doAction();
        parent.finish();
      }
    };

    gui = new FrameWork(bsInput, getWidth(), getHeight());
    gui.add(bttn_exit);
    gui.add(bttn_table);
    gui.add(bttn_manage);
    gui.validateUI();
  }

  @Override
  public void update(long elapsedTime) {
    gui.update();
  }

  @Override
  public void render(Graphics2D g) {
    g.setBackground(background_color);
    g.clearRect(0, 0, getWidth(), getHeight());
    gui.render(g);
  }
}
