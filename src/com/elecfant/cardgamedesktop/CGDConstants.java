/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elecfant.cardgamedesktop;

import java.awt.Color;

/**
 *
 * @author Drags
 */
public interface CGDConstants {


  //GLOBAL Constants

  //TODO normal jar_location path
  public final static String pre_assets = "assets/";
  public final static String pre_gui = pre_assets + "gui/";
  public final static Color background_color = new Color(230, 230, 230);
  public final static String cards_db_file = "cards.ddb";
}
