/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elecfant.cardgamedesktop;

import com.elecfant.cardgamedesktop.cards.CardList;
import com.golden.gamedev.GameEngine;
import com.golden.gamedev.GameObject;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;

/**
 *
 * @author Drags
 */
public class CGDEngine extends GameEngine implements CGDConstants {

  public CGDEngine() {
    this(false, 0);
  }

  CGDEngine(boolean debug, int start_state) {
    distribute = !debug;
    this.nextGameID = start_state;
  }

  @Override
  protected void initEngine() {
    super.initEngine();
    bsLoader.setBaseIO(bsIO);
    CardList.init(bsLoader, cards_db_file);
  }

  @Override
  public GameObject getGame(int GameID) {
    switch (GameID) {
      case 0:
        return new CGDMainMenu(this);
      case 1:
        return new CGDGame(this);
      case 2:
        return new CGDManage(this);
      default:
        return new CGDMainMenu(this);
    }
  }

  // global update sequence
  @Override
  public void update(long elapsedTime) {
    super.update(elapsedTime);
    if (bsInput.isKeyDown(KeyEvent.VK_ALT) && bsInput.isKeyDown(KeyEvent.VK_F4)) {
      this.finish();
    }
  }

  // global render sequence
  @Override
  public void render(Graphics2D g) {
    super.render(g);
  }
}
