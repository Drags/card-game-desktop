/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elecfant.cardgamedesktop;

import com.golden.gamedev.GameLoader;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Drags
 */
public class CardGameDesktop implements CGDConstants{

  public static boolean isDebug = false;
  public static int start_state = 0;

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    // command line parameters parsing
    for (int i = 0; i < args.length; ++i) {
      if ("-d".equalsIgnoreCase(args[i])) {
        isDebug = true;
      }

      if ("-g".equalsIgnoreCase(args[i])) {
        if (i + 1 < args.length) {
          ++i;
          try {
            start_state = Integer.decode(args[i]);
          } catch (NumberFormatException ex) {
            System.out.println("State code should be an integer: " + ex.getLocalizedMessage());
          }
        }
      }
    }
    GameLoader gLoader = new GameLoader();

    if (isDebug) {
      System.out.println("DEBUG MODE");
      File dir1 = new File(".");
      try {
        System.out.println("Current dir : " + dir1.getCanonicalPath());
      } catch (IOException ex) {
        Logger.getLogger(CardGameDesktop.class.getName()).log(Level.SEVERE, null, ex);
      }

      System.out.println("System properties:");
      for (Map.Entry<Object, Object> e : System.getProperties().entrySet()) {
        System.out.println(e);
      }
      gLoader.setup(new CGDEngine(isDebug, start_state), new Dimension(640, 480), false, true);
    } else {
      gLoader.setup(new CGDEngine(isDebug, start_state), Toolkit.getDefaultToolkit().getScreenSize(), true, true);
    }
    gLoader.start();
  }
}
