/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elecfant.cardgamedesktop.manage;

import com.elecfant.cardgamedesktop.cards.Card;
import com.elecfant.cardgamedesktop.cards.CardList;
import com.elecfant.dragslib.CallBack;
import com.golden.gamedev.gui.TButton;
import com.golden.gamedev.gui.TFloatPanel;
import com.golden.gamedev.gui.TLabel;
import com.golden.gamedev.gui.TPanel;
import com.golden.gamedev.gui.TTextField;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Drags
 */
public class CardEditPanel extends TFloatPanel {

  private final static FileNameExtensionFilter image_filter = new FileNameExtensionFilter("Image files (*.png, *.jpg, *.bmp)", "png", "jpg", "bmp");
  private Card card;
  private TTextField tf_title;
  private TTextField tf_fore;
  private TTextField tf_back;
  private TButton bttn_fore_browse;
  private TButton bttn_back_browse;
  private TTextField tf_description;
  private TButton bttn_OK;
  private TButton bttn_Cancel;
  private CallBack callback;

  private CardEditPanel(String title, boolean closable, boolean iconifiable, int x, int y, int w, int h) {
    super(title, closable, iconifiable, x, y, w, h);

  }

  public CardEditPanel(CallBack callback) {
    this();
    this.callback = callback;
  }

  // TODO fix all of the constructors. some of them calling each other and doing double job

  // TODO callback for card editing
  public CardEditPanel(Card card) {
    this();
    this.card = card;
    this.getTitleBar().setTitle("Edit Card - " + card.get_title());
    this.tf_title.setEditable(false);
    this.tf_title.setText(card.get_title());
    this.tf_description.setText(card.getDescription());
    this.tf_fore.setText(card.getFore());
    this.tf_back.setText(card.getBack());
  }

  /**
   * Initialize new card editing panel with new and empty card.
   */
  public CardEditPanel() {
    this("New Card - ", true, true, 0, 0, 500, 300);
    card = new Card();

    this.add(new TLabel("Title:", 0, 0, 100, 30));
    tf_title = new TTextField("", 100, 0, 300, 30) {
      @Override
      protected void processKeyReleased() {
        super.processKeyReleased();
        if (!CardEditPanel.this.getTitle().equals("New Card - " + this.getText())) {
          CardEditPanel.this.setTitle("New Card - " + this.getText());
        }
      }
    };
    this.add(tf_title);

    this.add(new TLabel("Foreground:", 0, 35, 100, 30));
    this.tf_fore = new TTextField("", 100, 35, 200, 30);
    this.tf_fore.setEditable(false);
    this.bttn_fore_browse = new TButton("Browse", 300, 35, 100, 30) {
      @Override
      public void doAction() {
        super.doAction();
        JFrame od = new JFrame();
        JFileChooser fileDialog = new JFileChooser();
        fileDialog.setDialogTitle("Select card foreground");
        fileDialog.setFileFilter(image_filter);
        int openChoice = fileDialog.showOpenDialog(od);
        if (openChoice == JFileChooser.APPROVE_OPTION) {
          try {
            CardEditPanel.this.tf_fore.setText(fileDialog.getSelectedFile().getCanonicalPath());
            CardEditPanel.this.tf_fore.setToolTipText(fileDialog.getSelectedFile().getCanonicalPath());
          } catch (IOException ex) {
            Logger.getLogger(CardEditPanel.class.getName()).log(Level.SEVERE, null, ex);
          }
        }
      }
    };
    this.add(tf_fore);
    this.add(bttn_fore_browse);

    this.add(new TLabel("Background:", 0, 70, 100, 30));
    this.tf_back = new TTextField("", 100, 70, 200, 30);
    this.tf_back.setEditable(false);
    this.bttn_back_browse = new TButton("Browse", 300, 70, 100, 30) {
      @Override
      public void doAction() {
        super.doAction();
        JFrame od = new JFrame();
        JFileChooser fileDialog = new JFileChooser();
        fileDialog.setDialogTitle("Select card background");
        fileDialog.setFileFilter(image_filter);
        int openChoice = fileDialog.showOpenDialog(od);
        if (openChoice == JFileChooser.APPROVE_OPTION) {
          try {
            CardEditPanel.this.tf_back.setText(fileDialog.getSelectedFile().getCanonicalPath());
            CardEditPanel.this.tf_back.setToolTipText(fileDialog.getSelectedFile().getCanonicalPath());
          } catch (IOException ex) {
            Logger.getLogger(CardEditPanel.class.getName()).log(Level.SEVERE, null, ex);
          }
        }
      }
    };
    this.add(this.tf_back);
    this.add(this.bttn_back_browse);

    this.add(new TLabel("Description:", 0, 105, 300, 30));
    this.tf_description = new TTextField("", 0, 140, 400, 100);
    this.add(this.tf_description);

    this.bttn_OK = new TButton("OK", 0, this.getHeight() - 30 - this.getTitleBar().getHeight(), 100, 30) {
      @Override
      public void doAction() {
        super.doAction();

        card.set_title(CardEditPanel.this.tf_title.getText());
        card.set_description(CardEditPanel.this.tf_description.getText());
        card.set_background_file(CardEditPanel.this.tf_fore.getText());
        card.set_foreground_file(CardEditPanel.this.tf_back.getText());
        CardList.add(card);
        if (CardEditPanel.this.callback != null) {
          CardEditPanel.this.callback.callback_action();
        }
        CardEditPanel.this.getTitleBar().getCloseButton().doAction();
      }
    };
    this.bttn_Cancel = new TButton("Cancel", this.getWidth() - 100, this.getHeight() - 30 - this.getTitleBar().getHeight(), 100, 30) {
      @Override
      public void doAction() {
        super.doAction();
        if (CardEditPanel.this.callback != null) {
          CardEditPanel.this.callback.callback_action();
        }
        CardEditPanel.this.getTitleBar().getCloseButton().doAction();
      }
    };
    this.add(bttn_OK);
    this.add(bttn_Cancel);

    this.validateUI();
  }


}
