/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elecfant.cardgamedesktop.cards;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.Collections;

/**
 *
 * @author Drags
 */
public class Deck extends GameEntity {

  private Card contents[] = null;
  private int width = 0;
  private int height = 0;
  private String title = "Deck";

  public void add_card(Card card) {
    if (contents == null) {
      contents = new Card[1];
    } else {
      Card[] temp = new Card[contents.length];
      System.arraycopy(contents, 0, temp, 0, contents.length);
      contents = new Card[contents.length + 1];
      System.arraycopy(temp, 0, contents, 0, temp.length);
    }
    contents[contents.length - 1] = card;
    if (card.get_height() > this.height || contents.length == 1) {
      this.height = card.get_image_height();
    }
    if (card.get_width() > this.width || contents.length == 1) {
      this.width = card.get_image_width();
    }
  }

  public void shuffle() {
    Collections.shuffle(Arrays.asList(contents));
  }

  public Card pop() {
    if (contents == null) {
      return null;
    }
    if (contents.length == 0) {
      return null;
    }
    Card result;
    result = contents[contents.length - 1];
    Card[] temp = new Card[contents.length - 1];
    System.arraycopy(contents, 0, temp, 0, contents.length - 1);
    contents = temp;
    result.set_scale(scale);
    result.set_pos(this.x, this.y);
    result.set_rot(this.rot);
    result.set_facing(false);
    return result;
  }

  @Override
  public void render(Graphics2D g) {
    BufferedImage img = null;
    if (this.contents != null) {
      if (this.contents.length != 0) {
        img = this.contents[this.contents.length - 1].get_background_image();
      }
    }
    if (img != null) {
      g.drawImage(img, new AffineTransform(Math.cos(rot) * this.scale, -Math.sin(rot), Math.sin(rot), Math.cos(rot) * this.scale, this.x, this.y), null);
    } else {
      g.setPaint(Color.WHITE);
      g.fillRect(this.x + 1, this.y + 1, (int) (this.width * this.scale) - 1, (int) (this.height * this.scale) - 1);
      g.setPaint(Color.BLACK);
      g.drawRect(this.x, this.y, (int) (this.width * this.scale), (int) (this.height * this.scale));
    }
    int label;
    if (this.contents != null) {
      label = contents.length;
    } else {
      label = 0;
    }
    g.setFont(new Font("Calibri", Font.PLAIN, (int) (100 * this.scale)));
    int font_width = g.getFontMetrics().stringWidth(String.valueOf(label));
    int font_height = g.getFontMetrics().getHeight();
    g.drawString(String.valueOf(label), (int) (this.x - font_width / 2 + this.width * this.scale / 2), (int) (this.y + +font_height / 2 + this.height * this.scale / 2));
    font_width = g.getFontMetrics().stringWidth(title);
    g.drawString(title, (int) (this.x - font_width / 2 + this.width * this.scale / 2), this.y + font_height);
  }

  @Override
  public int get_width() {
    return this.width;
  }

  @Override
  public int get_height() {
    return this.height;
  }

  public void set_size(int w, int h) {
    this.width = w;
    this.height = h;
  }
}
