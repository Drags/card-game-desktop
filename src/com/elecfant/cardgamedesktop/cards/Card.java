/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elecfant.cardgamedesktop.cards;

import com.elecfant.json.JSONArray;
import com.elecfant.json.JSONException;
import com.elecfant.json.JSONObject;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Drags
 */
public class Card extends GameEntity {

  private String title = "";
  private BufferedImage foreground = null;
  private BufferedImage background = null;
  private String back = "";
  private String fore = "";
  private boolean face_up = true;
  private String description = "";

  public Card(Card orig) {
    super(orig);
    this.title = orig.title;
    this.description = orig.description;
    this.foreground = orig.foreground;
    this.background = orig.background;
    this.face_up = orig.face_up;
  }

  public Card(String title, String description) {
    this.title = title;
    this.description = title;
  }

  public Card(String title, String description, BufferedImage foreground, BufferedImage background, boolean face_up) {
    super();
    this.title = title;
    this.description = description;
    this.foreground = foreground;
    this.background = background;
    this.face_up = face_up;
  }

  public Card(BufferedImage foreground, BufferedImage background, boolean face_up) {
    this(foreground, background);
    this.face_up = face_up;
  }

  public Card(BufferedImage foreground, BufferedImage background) {
    super();
    this.foreground = foreground;
    this.background = background;
  }

  public Card(JSONObject data) {
    try {
      this.title = data.getString("title");
      this.description = data.getString("description");
      this.back = data.getString("back");
      this.fore = data.getString("fore");
    } catch (JSONException ex) {
      Logger.getLogger(Card.class.getName()).log(Level.SEVERE, null, ex);
    }

  }

  public Card() {
    super();
  }

  public void flip() {
    this.face_up ^= true;
  }

  public void set_facing(boolean face_up) {
    this.face_up = face_up;
  }

  public void set_background(BufferedImage back) {
    this.background = back;
  }

  public void set_foreground(BufferedImage fore) {
    this.foreground = fore;
  }

  private BufferedImage get_drawable() {
    if (this.face_up) {
      return this.foreground;
    } else {
      return this.background;
    }
  }

  @Override
  public void render(Graphics2D g) {
    BufferedImage drawImg = this.get_drawable();
    if (drawImg != null) {
      g.drawImage(drawImg, new AffineTransform(Math.cos(rot) * this.scale, -Math.sin(rot), Math.sin(rot), Math.cos(rot) * this.scale, this.x, this.y), null);
    } else {
      g.drawRect(x, y, 300, 450);
    }
  }

  // TODO fix and unify get_[image]_<width/height> methods (maybe add width/height property to game entity)
  @Override
  public int get_width() {
    if (foreground == null) {
      return 0;
    } else {
      return this.foreground.getWidth();
    }
  }

  @Override
  public int get_height() {
    if (foreground == null) {
      return 0;
    } else {
      return this.foreground.getHeight();
    }
  }

  public int get_image_height() {
    BufferedImage drawable = this.get_drawable();
    if (drawable != null) {
      return this.get_drawable().getHeight();
    } else {
      return 0;
    }

  }

  public int get_image_width() {
    BufferedImage drawable = this.get_drawable();
    if (drawable != null) {
      return this.get_drawable().getWidth();
    } else {
      return 0;
    }

  }

  public BufferedImage get_background_image() {
    if (this.background != null) {
      return this.background;
    } else {
      return CardList.getDefaultCardBackground();
    }
  }

  public void set_title(String text) {
    this.title = text;
  }

  public void set_description(String text) {
    this.description = text;
  }

  public String get_title() {
    return this.title;
  }

  public JSONObject getJSONObject() {
    JSONObject result = new JSONObject();
    try {
      result.put("title", this.title);
      result.put("description", this.description);
      result.put("back", this.back);
      result.put("fore", this.fore);
    } catch (JSONException ex) {
      Logger.getLogger(Card.class.getName()).log(Level.SEVERE, null, ex);
    }
    return result;
  }

  public void set_background_file(String text) {
    this.back = text;
  }

  public void set_foreground_file(String text) {
    this.fore = text;
  }

  public String getBack() {
    return back;
  }

  public String getFore() {
    return fore;
  }

  public String getDescription() {
    return description;
  }

  public BufferedImage get_foreground_image() {
    return this.foreground;
  }
}
