/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elecfant.cardgamedesktop.cards;

import java.awt.Graphics2D;

/**
 *
 * @author Drags
 */
public abstract class GameEntity {

  protected int x = 0;
  protected int y = 0;
  protected double rot = 0;
  protected double scale = 1;

  public GameEntity(GameEntity orig) {
    this.x = orig.x;
    this.y = orig.y;
    this.rot = orig.rot;
    this.scale = orig.scale;
  }

  public GameEntity() {
    super();
  }

  public void set_pos(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public void set_rot(double rot) {
    this.rot = rot % (2 * Math.PI);
  }

  public void move(int dx, int dy) {
    this.x += dx;
    this.y += dy;
  }

  public void rotate(double angle) {
    this.rot = (this.rot + angle) % (2 * Math.PI);
  }

  public void set_scale(double scale) {
    this.scale = scale;
  }

  public void scale(double factor) {
    this.scale = this.scale * factor;
  }

  abstract public void render(Graphics2D g);

  abstract public int get_width();

  abstract public int get_height();

  public int get_scaled_width() {
    return (int) (this.get_width() * this.scale);
  }

  public int get_scaled_height() {
    return (int) (this.get_height() * this.scale);
  }

  public boolean inside(int x, int y) {
    return x > this.x && x < this.x + this.get_scaled_width() && y > this.y && y < this.y + this.get_scaled_height();
  }
}
