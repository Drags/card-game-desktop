/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elecfant.cardgamedesktop.cards;

import com.elecfant.cardgamedesktop.CGDConstants;
import com.elecfant.json.JSONArray;
import com.elecfant.json.JSONException;
import com.elecfant.json.JSONObject;
import com.golden.gamedev.engine.BaseLoader;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Drags
 */
public class CardList implements CGDConstants {

  static private BufferedImage defaultCardImage = null;
  static private BufferedImage defaultCardBack = null;
  static private BaseLoader bsLoader;
  static private int iDB = 0;
  static private JSONObject jObject = null;
  static private boolean initialized = false;

  static public BufferedImage getDefaultCardImage() {
    return defaultCardImage;
  }

  static public Card create_card(String filename, String backname) {
    Card temp = new Card(bsLoader.getImage(filename), bsLoader.getImage(backname));
    try {
      if (!jObject.has("Cards")) {
        jObject.put("Cards", new JSONArray());
      }
      jObject.getJSONArray("Cards").put(temp);
      save_database(cards_db_file);
    } catch (JSONException ex) {
      Logger.getLogger(CardList.class.getName()).log(Level.SEVERE, null, ex);
    }
    return temp;
  }

  static private String readFileAsString(String filePath) throws IOException {
    StringBuilder fileData = new StringBuilder();
    try (BufferedReader reader = new BufferedReader(
                    new FileReader(filePath))) {
      char[] buf = new char[1024];
      int numRead;
      while ((numRead = reader.read(buf)) != -1) {
        String readData = String.valueOf(buf, 0, numRead);
        fileData.append(readData);
      }
    }
    return fileData.toString();
  }

  static public void set_defaults(String fore, String back) {
    if (back != null) {
      defaultCardBack = bsLoader.getImage(back);
    } else {
      defaultCardBack = null;
    }
    if (fore != null) {
      defaultCardImage = bsLoader.getImage(fore);
    } else {
      defaultCardImage = null;
    }

  }

  static public Deck deck_from_all() {
    Deck result = new Deck();
    for (int i = 0; i < iDB; ++i) {
      // result.add_card(new Card(cardDB[i]));
    }
    return result;
  }

  static public void init(BaseLoader bsLoader, String filePath) {
    CardList.bsLoader = bsLoader;
    try {
      jObject = new JSONObject(readFileAsString(filePath));
    } catch (IOException | JSONException ex) {
      System.out.println("WARN: Database not existant or corrupted. Creating blank.");
      jObject = new JSONObject();
      try {
        jObject.put("Cards", new JSONArray());
      } catch (JSONException ex1) {
        Logger.getLogger(CardList.class.getName()).log(Level.SEVERE, null, ex1);
      }
      save_database(filePath);
    }

    initialized = true;
  }

  static public void save_database(String path) {
    if (jObject == null) {
      System.out.println("ERROR: Can't save null JSON Object");
      return;
    }
    try {
      try (FileWriter fw = new FileWriter(path)) {
        jObject.write(fw);
      }
    } catch (JSONException | IOException ex) {
      Logger.getLogger(CardList.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  public static BufferedImage getDefaultCardBackground() {
    return defaultCardBack;
  }

  public static void add(Card card) {
    try {
      if (!jObject.has("Cards")) {
        jObject.put("Cards", new JSONArray());
      }
      jObject.getJSONArray("Cards").put(card.getJSONObject());
      save_database(cards_db_file);
    } catch (JSONException ex) {
      Logger.getLogger(CardList.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  public static int getCardQuantity() {
    JSONArray array;
    try {
      array = jObject.getJSONArray("Cards");
    } catch (JSONException ex) {
      System.out.println("ERROR: No Cards JSON entry. Creating new.");
      return 0;
    }
    return array.length();
  }

  public static Card getCard(int i) {
    Card result;
    try {
      result = new Card((JSONObject) jObject.getJSONArray("Cards").get(i));
    } catch (JSONException ex) {
      Logger.getLogger(CardList.class.getName()).log(Level.SEVERE, null, ex);
      return null;
    }
    return result;
  }

  public static Card getCardByName(String selected_card) {
    JSONArray array = null;
    try {
      array = jObject.getJSONArray("Cards");
    } catch (JSONException ex) {
      Logger.getLogger(CardList.class.getName()).log(Level.SEVERE, null, ex);
    }
    for (int i = 0; i < array.length(); ++i) {
      Card card = null;
      try {
        card = new Card(array.getJSONObject(i));
      } catch (JSONException ex) {
        Logger.getLogger(CardList.class.getName()).log(Level.SEVERE, null, ex);
      }
      if (card.get_title().equalsIgnoreCase(selected_card)) {
        return card;
      }
    }
    return null;
  }
}