/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elecfant.cardgamedesktop;

import com.elecfant.cardgamedesktop.cards.Card;
import com.elecfant.cardgamedesktop.cards.CardList;
import com.elecfant.cardgamedesktop.manage.CardEditPanel;
import com.elecfant.dragslib.CallBack;
import com.golden.gamedev.GameObject;
import com.golden.gamedev.gui.TButton;
import com.golden.gamedev.gui.TListBox;
import com.golden.gamedev.gui.toolkit.FrameWork;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;

/**
 *
 * @author Drags
 */
class CGDManage extends GameObject implements CGDConstants {

  private FrameWork gui;
  private TListBox listBox;

  public CGDManage(CGDEngine aThis) {
    super(aThis);
    setFPS(30);
  }

  @Override
  public void initResources() {
    gui = new FrameWork(bsInput, getWidth(), getHeight());

    listBox = new TListBox(getWidth() - 200, 0, 200, getHeight()) {
      @Override
      protected void processMouseDoubleClicked() {
        super.processMouseDoubleClicked();
        String selected_card = this.contents[this.selected];
        Card card = CardList.getCardByName(selected_card);
        CGDManage.this.gui.add(new CardEditPanel(card));
        CGDManage.this.gui.validateUI();

      }
    };
    for (int i = 0; i < CardList.getCardQuantity(); ++i) {
      listBox.add(CardList.getCard(i).get_title());
    }
    gui.add(listBox);

    TButton bttn_back = new TButton("Back", 30, getHeight() - 70, 100, 40) {
      @Override
      public void doAction() {
        super.doAction();
        parent.nextGameID = 0;
        CGDManage.this.finish();
      }
    };
    gui.add(bttn_back);
    TButton bttn_add_card;
    bttn_add_card = new TButton("Add Card", 30, 30, 100, 40) {
      @Override
      public void doAction() {
        super.doAction();
        CGDManage.this.gui.add(new CardEditPanel(new CallBack() {
          @Override
          public void callback_action() {
            CGDManage.this.refresh_card_list();
          }

          @Override
          public void callback_action(int param) {
            this.callback_action();
          }

          @Override
          public void callback_action(String param) {
            this.callback_action();
          }
        }));
        CGDManage.this.gui.validateUI();
      }
    };
    gui.add(bttn_add_card);

    gui.validateUI();
  }

  @Override
  public void update(long elapsedTime) {
    if (bsInput.isKeyReleased(KeyEvent.VK_ESCAPE)) {
      this.parent.nextGameID = 0;
      this.finish();
    }

    gui.update();
  }

  @Override
  public void render(Graphics2D g) {
    // clearing and background
    g.setBackground(background_color);
    g.clearRect(0, 0, getWidth(), getHeight());
    // --

    // gui
    gui.render(g);

    // overlay
    int x = bsInput.getMouseX();
    int y = bsInput.getMouseY();
    if (listBox.intersects(x, y)) {
      String card_name = listBox.getString(listBox.getIndex(x, y));
      if (card_name != null) {
        Card card = CardList.getCardByName(card_name);
        int height = (int)Math.round(100.0 / bsLoader.getImage(card.getFore()).getWidth() * bsLoader.getImage(card.getFore()).getHeight());
        g.drawImage(bsLoader.getImage(card.getFore()), x, y, 100, height, null);
        g.drawImage(bsLoader.getImage(card.getBack()), x, y+height, 100, height, null);
      }
    }
    // --
  }

  private void refresh_card_list() {
    listBox.removeAll();
    for (int i = 0; i < CardList.getCardQuantity(); ++i) {
      listBox.add(CardList.getCard(i).get_title());
    }
  }
}
